import 'package:timeago/timeago.dart' as timeago;

class TimeAgo {

  String timeAgoSinceDate(String dateString, {bool numericDates = true}) {
    DateTime date = DateTime.parse(dateString);
    final now = DateTime.now();
    final difference = now.difference(date);
    final dateText = timeago.format(now.subtract(difference));
    return dateText;
  }
}