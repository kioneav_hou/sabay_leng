import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DialogLoading {

  void dialogLoading () {
    Get.dialog(
      const Center(
        child: CircularProgressIndicator(),
      ),
      barrierDismissible: false,
    );
  }

  void dialogDismiss () {
    Get.back();
  }
}