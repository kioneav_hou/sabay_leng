import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sabay_leng/controllers/post_controller.dart';
import 'package:sabay_leng/screens/component/upload_post_screen.dart';

import '../../models/edit_post_model.dart';

class HeaderPost extends StatelessWidget {
  HeaderPost({
    super.key,
  });
  final PostController postController = Get.put(PostController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10),
      child: Row(
        children: [
          InkWell(
            borderRadius: BorderRadius.circular(20),
            onTap: () {},
            child: const CircleAvatar(
              backgroundImage: NetworkImage('https://i.pinimg.com/236x/81/d0/87/81d087094d5653243df2f9ec616a4594.jpg'),
              radius: 20,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: InkWell(
              borderRadius: BorderRadius.circular(20),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.65,
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(
                    color: Colors.grey,
                    width: 1,
                  ),
                ),
                child: const Text("What's on your mind?"),
              ),
              onTap: (){
                postController.editPosts.value = EditPost();
                Get.to(() => const UploadPostScreen(), fullscreenDialog: true);
              },
            ),
          ),
          const Spacer(),
          IconButton(
            onPressed: () {
              postController.editPosts.value = EditPost();
              Get.to(() => const UploadPostScreen(), fullscreenDialog: true);
            },
            icon: const Icon(Icons.photo),
          ),
        ],
      ),
    );
  }
}