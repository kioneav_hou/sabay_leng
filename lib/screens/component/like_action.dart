import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sabay_leng/controllers/user_global_controller.dart';

import '../../controllers/comment_controller.dart';
import '../../controllers/post_controller.dart';
import 'comments/model_comment.dart';

class ActionLike extends StatefulWidget {
  const ActionLike({
    super.key,
    required this.postController,
    required this.data,
    required this.index,
    required this.imageUrlProfile,
  });

  final PostController postController;
  final List<dynamic> data;
  final int index;
  final String? imageUrlProfile;

  @override
  State<ActionLike> createState() => _ActionLikeState();
}

class _ActionLikeState extends State<ActionLike> {
  final CommentController comController = Get.put(CommentController());
  final UserGlobalController userGlobalController = Get.put(UserGlobalController());
  var _likeColor;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment
          .spaceAround,
      children: [
        Row(
          children: [
            widget.postController.obx((data) =>
                IconButton(
                  onPressed: () async {
                    if(data!.data![widget.index].liked == true) {
                      setState(() {
                        _likeColor = false;
                      });
                    }else{
                      setState(() {
                        _likeColor = true;
                      });
                    }
                    data!.data![widget.index].liked == true ? await widget.postController.unLikePost(data!.data![widget.index].id!) : await widget.postController.likePost(data!.data![widget.index].id!);
                  },
                  icon: _likeColor == true ? const Icon(Icons.favorite, color: Colors.blueAccent,)
                      : _likeColor == false ? const Icon(Icons.favorite_border)
                      : data!.data![widget.index].liked == true ? const Icon(Icons.favorite, color: Colors.blueAccent,)
                      : const Icon(Icons.favorite_border),
                ),
            ),
            Text('Like'),

          ],
        ),
        Row(
          children: [
            IconButton(
              onPressed: () async {
                //create floating screen for comment
                if(widget.data![widget.index!]!.id! != null) {
                  await comController.getComment(widget.data![widget.index].id!);
                  comController.postId = widget.data![widget.index].id!;
                  Get.to(PreviewPost(postId: widget.data![widget.index!]!.id!), transition: Transition.downToUp);
                }
              },
              icon: const Icon(Icons.comment),
            ),
            const Text('Comment'),
          ],
        ),
        Row(
          children: [
            IconButton(
              onPressed: () async {
                await userGlobalController.comingSoon();
              },
              icon: const Icon(Icons.share),
            ),
            const Text('Share'),
          ],
        ),
      ],
    );
  }
}