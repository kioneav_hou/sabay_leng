import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sabay_leng/controllers/post_controller.dart';
import 'package:sabay_leng/models/edit_post_model.dart';
import 'package:transparent_image/transparent_image.dart';

class UploadPostScreen extends StatefulWidget {
  const UploadPostScreen({Key? key}) : super(key: key);

  @override
  State<UploadPostScreen> createState() => _UploadPostScreenState();
}

class _UploadPostScreenState extends State<UploadPostScreen> {
  final TextEditingController _textPostController = TextEditingController(
    text: Get.find<PostController>().editPosts.value.body ?? "",
  );
  final PostController postController = Get.put(PostController());


  @override
  void dispose() {
    _textPostController.dispose();
    postController.editPosts.value = EditPost();
    postController.imagePath = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create post"),
        actions: [
          TextButton(
            onPressed: () async {
              if(postController.editPosts.value.image == null && postController.editPosts.value.id == null){
                if(postController.imagePath == null){
                  Get.defaultDialog(
                    title: "Warning",
                    middleText: "Please select image",
                    textConfirm: "OK",
                    confirmTextColor: Colors.white,
                    onConfirm: (){
                      Get.back();
                    },
                  );
                  return;
                }
                await postController.postImage(_textPostController.text);
              }else{
                await postController.updatePost(_textPostController.text);
              }
            },
            child: Text("Post".toUpperCase(), style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.normal,),),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                const Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CircleAvatar(
                      backgroundImage: NetworkImage('https://i.pinimg.com/236x/81/d0/87/81d087094d5653243df2f9ec616a4594.jpg'),
                      radius: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Sokheng", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                    ),
                  ],
                ),
                Row(
                 children: [
                   Expanded(
                      child: TextField(
                        controller: _textPostController,
                        decoration: const InputDecoration(
                          hintText: "What's on your mind?",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                 ],
                ),
                if(postController.editPosts.value.image != null)
                    Stack(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.5,
                          width: MediaQuery.of(context).size.width,
                          child: FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            image: postController.editPosts!.value!.image!,
                            fit: BoxFit.cover,
                          )
                        ),
                        Positioned(
                          right: 0,
                          child: IconButton(
                            onPressed: () async {
                              await postController.cancelImageEditPost();
                              setState(() {

                              });
                            },
                            icon: Icon(Icons.cancel),
                          ),
                        ),
                      ],
                    )
                else
                  postController.imagePath != null ?
                    Stack(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.5,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: FileImage(postController.imagePath),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Positioned(
                          right: 0,
                          child: IconButton(
                            onPressed: () async {
                              await postController.cancelImagePost();
                              setState(() {

                              });
                            },
                            icon: Icon(Icons.cancel),
                          ),
                        ),
                      ],
                    )
                  :
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                  ),
                ListTile(
                  onTap: () async {
                    await postController.getLostData();
                    setState(() {

                    });
                  },
                  leading: Icon(Icons.photo),
                  title: Text("Photo"),
                ),
                ListTile(
                  onTap: (){},
                  leading: Icon(Icons.video_call),
                  title: Text("Video"),
                ),
                ListTile(
                  onTap: (){},
                  leading: Icon(Icons.location_on),
                  title: Text("Check in"),
                ),
                ListTile(
                  onTap: (){},
                  leading: Icon(Icons.emoji_emotions),
                  title: Text("Feeling/Activity"),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
}
