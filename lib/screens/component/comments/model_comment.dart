import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sabay_leng/controllers/comment_controller.dart';
import 'package:sabay_leng/controllers/user_global_controller.dart';
import 'package:sabay_leng/screens/component/comments/list_comments.dart';
import 'package:sabay_leng/screens/component/comments/reply_comment.dart';
import 'package:sabay_leng/screens/component/time_ago.dart';
import 'package:sabay_leng/controllers/post_controller.dart';
import 'package:sabay_leng/screens/component/comments/action_comment.dart';
import 'package:sabay_leng/services/api_references.dart';

import 'list_reply_comment.dart';

class PreviewPost extends StatefulWidget {
  const PreviewPost({Key? key, required this.postId}) : super(key: key);
  final int postId;

  @override
  State<PreviewPost> createState() => _PreviewPostState();
}

class _PreviewPostState extends State<PreviewPost> {
  final PostController postController = Get.put(PostController());
  final CommentController comController = Get.put(CommentController());
  final UserGlobalController userGlobalController = Get.put(UserGlobalController());
  final ActionComment actionComment = Get.put(ActionComment());
  final _formKeyComment = GlobalKey<FormState>();
  final TimeAgo timeAgo = TimeAgo();
  final ReferenceApi referenceApi = ReferenceApi();

  @override
  void initState() {
    comController.getComment(widget.postId!);
    super.initState();
  }

  @override
  void dispose() {
    comController.postId = 0;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () async {
            Get.back();
          },
          icon: const Icon(Icons.arrow_back, color: Colors.black,),
        ),
        title: Center(
            child: comController.dataCommentPost != null ?
            comController.obx((state) => Text(comController.dataCommentPost!.commentCount! > 1 ?
            "${comController.dataCommentPost!.commentCount!} Comments" :
            "${comController.dataCommentPost!.commentCount!} Comment", style: const TextStyle(color: Colors.black, fontSize: 14),))
                :
            const Text("0 Comment", style: TextStyle(color: Colors.black, fontSize: 14),)
        ),
        actions: [
          IconButton(
            onPressed: (){

            },
            icon: const Icon(Icons.search, color: Colors.black,),
          ),
        ],
        //add line gray

        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GetBuilder<CommentController>(
              init: CommentController(),
              builder: (controller) {
                if(controller.dataCommentPost.data == null){
                  return const Center(
                    child: Text("No Comment"),
                  );
                }
                return ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: controller.dataCommentPost!.data!.length!,
                  itemBuilder: (context, indexComment) {
                    // if(controller.dataCommentPost == null){
                    //   return Container(
                    //     child: const Center(
                    //       child: CircularProgressIndicator(),
                    //     ),
                    //   );
                    // }
                    // if(controller.dataCommentPost.data == null){
                    //   return Container(
                    //     child: const Center(
                    //       child: Text("No Comment"),
                    //     ),
                    //   );
                    // }
                    // if(controller.dataCommentPost.data!.isEmpty){
                    //   return const Column(
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     crossAxisAlignment: CrossAxisAlignment.center,
                    //     children: [
                    //       Center(
                    //         child: Text("No Comment"),
                    //       ),
                    //     ],
                    //   );
                    // }
                    if(controller.dataCommentPost.data!.isNotEmpty){
                      var data = controller.dataCommentPost.data;
                      var imageUrlProfile = data![indexComment].user!.image == null? "https://i.stack.imgur.com/l60Hf.png" : data![indexComment].user!.image!.contains("http") ? data[indexComment].user!.image! : "${referenceApi.baseUrlUser}${data![indexComment].user?.image}";
                      print("data comment: ${controller.dataCommentPost.data!.length} ");
                      if(controller.dataCommentPost.data!.isNotEmpty){
                        var comment = controller.dataCommentPost.data;
                        return Column(
                          mainAxisAlignment: MainAxisAlignment
                              .start,
                          crossAxisAlignment: CrossAxisAlignment
                              .start,
                          children: [

                            ListComments(
                              comment: controller.dataCommentPost,
                              index: indexComment,
                              imageUrlProfile: imageUrlProfile!,
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            // create reply comment
                            Padding(
                              padding: const EdgeInsets
                                  .only(
                                // top: 1.0,
                                  left: 85.0,
                                  right: 16.0),
                              child: Row(
                                children: [
                                  GestureDetector(
                                    onTap: (){

                                    },
                                    child: const Text(
                                      "Like",
                                      style: TextStyle(
                                        fontSize: 16,

                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 20,),
                                  GestureDetector(
                                    onTap: () async {
                                      await comController.getReplyComment(comment![indexComment].id!);
                                      if(comment![indexComment].id != null){
                                        comController.commentId = comment![indexComment].id!;
                                        var response = await Get.to(ListReplyComment(imageUrlProfile: imageUrlProfile!, index: indexComment!, commentId: comment![indexComment].id!, comments: controller.dataCommentPost, timeAgo: timeAgo), transition: Transition.downToUp);
                                        if(response == true){
                                          await comController.getComment(widget.postId);
                                        }
                                      }
                                    },
                                    child: const Text(
                                      "Reply",
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            for(int i = 0; i < comment![indexComment].replies!.length!; i++)
                              ReplyComment(comment: controller.dataCommentPost, index: indexComment, i: i, timeAgo: timeAgo),
                          ],
                        );
                      }else{
                        return const Center(
                          child: Text("No Comment"),
                        );
                      }
                    }
                  }
                );
              }
            ),
            const SizedBox(
              height: 60,
            )
          ],
        ),
      ),
      bottomSheet:Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,),
        child: Row(
          children: [
            Expanded(
              child: Form(
                key: _formKeyComment,
                child: TextFormField(
                  keyboardType: TextInputType.multiline,
                  controller: comController.comment,
                  decoration: const InputDecoration(
                    hintText: "Write a comment...",
                    border: InputBorder
                        .none,

                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter your comment';
                    }
                    return null;
                  },
                ),
              ),
            ),
            //add emoji
            IconButton(
              onPressed: () {

              },
              icon: const Icon(
                  Icons
                      .emoji_emotions_outlined),
            ),
            IconButton(
              onPressed: () async {
                if(comController.comment.text.isEmpty){
                  return;
                }
                if(_formKeyComment.currentState!.validate()){
                  if(comController.dataComments.isEmpty){
                    await comController.postComment(widget.postId, comController.comment.text);
                  }else{
                    await comController.updateComment(comController.dataComments!["id"], comController.comment.text);
                  }
                  comController.comment.clear();
                }
              },
              icon: const Icon(
                  Icons
                      .send),
            ),
          ],
        ),
      ),
    );
  }
}

