import 'package:flutter/material.dart';
import 'package:sabay_leng/controllers/comment_controller.dart';
import 'package:sabay_leng/controllers/user_global_controller.dart';
import 'package:sabay_leng/models/comment_model.dart';
import 'package:sabay_leng/screens/component/time_ago.dart';
import 'package:get/get.dart';

import 'action_comment.dart';

class ListComments extends StatefulWidget {
  const ListComments({Key? key, this.imageUrlProfile, required this.index, required this.comment}) : super(key: key);
  final String? imageUrlProfile;
  final int index;
  final CommentModel comment;

  @override
  _ListCommentsState createState() => _ListCommentsState();
}

class _ListCommentsState extends State<ListComments> {
  final TimeAgo timeAgo = TimeAgo();
  final CommentController comController = Get.put(CommentController());
  final UserGlobalController userGlobalController = Get.put(UserGlobalController());
  final ActionComment actionComment = Get.put(ActionComment());

  @override
  Widget build(BuildContext context) {
    var comment = widget.comment;
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(
                widget.imageUrlProfile ?? "https://i.stack.imgur.com/l60Hf.png"),
            radius: 25,
          ),
          title: Column(
            mainAxisAlignment: MainAxisAlignment
                .start,
            crossAxisAlignment: CrossAxisAlignment
                .start,
            children: [
              Text(
                  comment.data![widget.index].user!.name   ?? ""),
              Text(
                timeAgo.timeAgoSinceDate(comment.data![widget.index].createdAt!), style: const TextStyle(fontSize: 14),),
            ],
          ),
          subtitle: Padding(
            padding: const EdgeInsets.only(
              top: 5.0,),
            child: GestureDetector(
              onTap: () {
                comController.seeMore(widget.index);
                setState(() {

                });
              },
              child: (comController.isSeeMore == true && comController.isSeeMoreIndex == widget.index) ? Text(
                comment.data![widget.index].text!,
                style: const TextStyle(
                  fontSize: 16,
                ),
              ) : Text(
                comment.data![widget.index].text!,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontSize: 16,
                ),
              ),
            ),
          ),

          trailing: IconButton(
            onPressed: ( ) async {
              if(await userGlobalController.isOwner(comment.data![widget.index].userId!)){
                await actionComment.showActionComment(id: comment.data![widget.index].id!);
              }
            },
            icon: const Icon(
                Icons
                    .more_horiz),
          )
      ),
    );
  }

}