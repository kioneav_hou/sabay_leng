import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/comment_controller.dart';

class ActionComment {
  final CommentController comController = Get.put(CommentController());


  Future<void> showActionComment({required int id}) async {
    await showModalBottomSheet(
      useSafeArea: true,
      useRootNavigator: true,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius
            .vertical(
          top: Radius.circular(15),
        ),
      ),
      context: Get.context!,
      builder: (context) {
        return SizedBox(
          height: MediaQuery
              .of(context)
              .size
              .height * 0.2,
          child: Column(
            children: [
              ListTile(
                leading: const Icon(Icons.edit),
                title: const Text("Edit"),
                onTap: () async {
                  await comController.editComment(id!, context);
                },
              ),
              ListTile(
                leading: const Icon(Icons.delete),
                title: const Text("Delete"),
                onTap: () async {
                  await comController.deleteComment(id!);
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
