import 'package:flutter/material.dart';
import 'package:sabay_leng/controllers/comment_controller.dart';
import 'package:sabay_leng/models/comment_model.dart';
import 'package:get/get.dart';

import '../time_ago.dart';
import 'list_comments.dart';

class ListReplyComment extends StatefulWidget {
  const ListReplyComment({Key? key, required this.comments, required this.index, required this.commentId, required this.imageUrlProfile, required this.timeAgo}) : super(key: key);
  final String imageUrlProfile;
  final int index;
  final int commentId;
  final CommentModel comments;
  final TimeAgo timeAgo;


  @override
  State<ListReplyComment> createState() => _ListReplyCommentState();
}

class _ListReplyCommentState extends State<ListReplyComment> {
  final CommentController comController = Get.put(CommentController());
  final _formKeyReply = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Get.back(result: true);
          },
          icon: const Icon(Icons.arrow_back, color: Colors.black,),
        ),
        title: const Center(child: Text("Replies", style: TextStyle(color: Colors.black, fontSize: 14),)),
        actions: [
          IconButton(
            onPressed: (){

            },
            icon: const Icon(Icons.search, color: Colors.black,),
          ),
        ],
        //add divider
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              const Divider(
                height: 1,
                thickness: 1,
              ),
              ListComments(
                imageUrlProfile: widget.imageUrlProfile,
                index: widget.index,
                comment: widget.comments,
              ),
              //reply comment
              Padding(
                padding: const EdgeInsets
                    .only(
                  // top: 1.0,
                    left: 85.0,
                    right: 16.0),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: (){

                      },
                      child: const Text(
                        "Like",
                        style: TextStyle(
                          fontSize: 16,

                          color: Colors.grey,
                        ),
                      ),
                    ),
                    const SizedBox(width: 20,),
                    GestureDetector(
                      onTap: () async {
                        await comController.getReplyComment(widget.commentId);
                        if(widget.commentId != null){
                          comController.commentId = widget.commentId;
                        }
                      },
                      child: const Text(
                        "Reply",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //each reply comment
              comController.obx((state) {
                if(comController.dataReplyComment.data!.isEmpty){
                  return Container();
                } else {
                  var dataReplyComment = comController.dataReplyComment.data!;
                  return Column(
                    children: List.generate(dataReplyComment.length, (index) {
                      return Padding(
                        padding: const EdgeInsets.only(
                          // top: 1.0,
                            left: 60.0,
                            right: 16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment
                              .start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ListTile(
                              leading: CircleAvatar(

                                backgroundImage: NetworkImage(
                                    widget.imageUrlProfile!.toString() ?? "https://i.stack.imgur.com/l60Hf.png"),
                                radius: 20,
                              ),
                              title: Column(
                                mainAxisAlignment: MainAxisAlignment
                                    .start,
                                crossAxisAlignment: CrossAxisAlignment
                                    .start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                      dataReplyComment[index].user!.name ?? ""),
                                  Text(
                                    widget.timeAgo.timeAgoSinceDate(
                                        dataReplyComment[index].createdAt!) ?? "",
                                    style: const TextStyle(fontSize: 14),),
                                ],
                              ),
                              subtitle: GestureDetector(
                                onTap: () {
                                  comController.seeMore(index);
                                },
                                child: (comController.isSeeMore == true && comController.isSeeMoreIndex == index) ?
                                  Text(
                                    dataReplyComment[index].text ?? "",
                                    maxLines: 100,
                                    style: const TextStyle(fontSize: 14),
                                  )
                                 :
                                  Text(
                                    dataReplyComment[index].text ?? "",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(fontSize: 14),
                                  ),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                      );
                    }),
                  );
                }
              }),
              const SizedBox(height: 60,),
            ],
          ),
        ),
      ),
      bottomSheet: Padding(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,),
        child: Row(
          children: [
            Expanded(
              child: Form(
                key: _formKeyReply,
                child: TextFormField(
                  focusNode: comController.focusNode,
                  keyboardType: TextInputType.multiline,
                  controller: comController.comment,
                  decoration: const InputDecoration(
                    hintText: "Write a comment...",
                    border: InputBorder
                        .none,

                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter your comment';
                    }
                    return null;
                  },
                ),
              ),
            ),
            //add emoji
            IconButton(
              onPressed: () {

              },
              icon: const Icon(
                  Icons
                      .emoji_emotions_outlined),
            ),
            IconButton(
              onPressed: () async {
                if(comController.comment.text.isEmpty){
                  return;
                }
                if(_formKeyReply.currentState!.validate()){
                  await comController.replyComment(widget.comments.data![widget.index].id, comController.comment.text, context);
                  setState(() {

                  });
                  comController.comment.clear();
                }
              },
              icon: const Icon(
                  Icons
                      .send),
            ),
          ],
        ),
      ),
    );
  }
}


