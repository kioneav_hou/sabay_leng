import 'package:flutter/material.dart';
import 'package:sabay_leng/models/comment_model.dart';
import 'package:sabay_leng/screens/component/time_ago.dart';
import 'package:sabay_leng/services/api_references.dart';

class ReplyComment extends StatefulWidget {
  const ReplyComment({
    super.key,
    required this.comment,
    required this.index,
    required this.i,
    required this.timeAgo,
  });

  final CommentModel comment;
  final int i;
  final int index;
  final TimeAgo timeAgo;

  @override
  State<ReplyComment> createState() => _ReplyCommentState();
}

class _ReplyCommentState extends State<ReplyComment> {
  bool isSeeMore = false;
  final ReferenceApi referenceApi = ReferenceApi();
  late final comments = widget.comment!.data![widget.index];
  late final imageUsers = comments.replies![widget.i].user!.image!;
  late final imageUrlProfile = imageUsers == null? "https://i.stack.imgur.com/l60Hf.png" :imageUsers!.contains("http") ? imageUsers! : "${referenceApi.baseUrlUser}$imageUsers";

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets
          .only(
        // top: 1.0,
          left: 60.0,
          right: 16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment
            .start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            leading: CircleAvatar(

              backgroundImage: NetworkImage(
                  comments.replies![widget.i].user!.image != null ? "${referenceApi.baseUrlUser}${comments.replies![widget.i].user!.image}" : "https://i.stack.imgur.com/l60Hf.png"),
              radius: 20,
            ),
            title: Column(
              mainAxisAlignment: MainAxisAlignment
                  .start,
              crossAxisAlignment: CrossAxisAlignment
                  .start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                    comments.replies![widget.i]!.user!.name! ?? ""),
                Text(
                  widget.timeAgo.timeAgoSinceDate(
                      comments.replies![widget.i].createdAt!),
                  style: const TextStyle(fontSize: 14),),
              ],
            ),
            subtitle: GestureDetector(
              onTap: () {
                setState(() {
                  isSeeMore = !isSeeMore;
                });
              },
              child: isSeeMore == true ?
              Text(
                  comments.replies![widget.i]!.text ?? "",
                  maxLines: 100,
                  style: const TextStyle(
                    fontSize: 16,
                  ),

              ) : Text(
                  comments.replies![widget.i]!.text ?? "",
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontSize: 16,
                  ),

              ),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          // create reply comment
          Padding(
            padding: const EdgeInsets
                .only(
                left: 75.0,
                // top: 1.0,
                right: 16.0),
            child: Row(
              children: [
                GestureDetector(
                  onTap: (){

                  },
                  child: const Text(
                    "Edit",
                    style: TextStyle(
                      fontSize: 16,

                      color: Colors.grey,
                    ),
                  ),
                ),
                const SizedBox(width: 20,),
                GestureDetector(
                  onTap: (){

                  },
                  child: const Text(
                    "Delete",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}