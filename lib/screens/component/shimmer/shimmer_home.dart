import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import 'content_post.dart';
import 'header_post_shimmer.dart';

class LoadingListPage extends StatefulWidget {
  const LoadingListPage({super.key});

  @override
  State<LoadingListPage> createState() => _LoadingListPageState();
}

class _LoadingListPageState extends State<LoadingListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Shimmer.fromColors(
          baseColor: Colors.grey.shade300,
          highlightColor: Colors.grey.shade100,
          enabled: true,
          child: SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.14,
                  width: double.infinity,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: 5,
                      itemBuilder: (context, index){
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 5.0),
                          child: Column(
                            children: [
                              InkWell(
                                onTap: () {

                                },
                                child: const CircleAvatar(
                                  backgroundColor: Colors.white,
                                  radius: 30,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 5.0),
                                child: Container(
                                  width: 50.0,
                                  height: 20.0,
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                  ),
                ),
                const SimmerHeaderPost(),
                const ContentPost(),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                ),
                const SimmerHeaderPost(),
                const ContentPost(),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                ),
                const SimmerHeaderPost(),
                const ContentPost(),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                ),
                const SimmerHeaderPost(),
                const ContentPost(),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                ),
                const SimmerHeaderPost(),
                const ContentPost(),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                ),
                const SimmerHeaderPost(),
                const ContentPost(),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                ),
                const SimmerHeaderPost(),
                const ContentPost(),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                ),
                const SimmerHeaderPost(),
                const ContentPost(),
              ],
            ),
          )),
    );
  }
}

