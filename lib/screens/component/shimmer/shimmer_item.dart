import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import 'content_post.dart';
import 'header_post_shimmer.dart';

class LoadingItemPost extends StatefulWidget {
  const LoadingItemPost({super.key});

  @override
  State<LoadingItemPost> createState() => _LoadingItemPostState();
}

class _LoadingItemPostState extends State<LoadingItemPost> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey.shade300,
      highlightColor: Colors.grey.shade100,
      enabled: true,
      child: const Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          SimmerHeaderPost(),
          ContentPost(),
        ],
      ),
    );
  }

}