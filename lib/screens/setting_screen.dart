import 'package:flutter/material.dart';
import 'package:sabay_leng/controllers/auth_controller.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          //create list setting like facebook
          child: Container(
            child: Column(
              children: [
                const ListTile(
                  leading: Icon(Icons.person),
                  title: Text("Account"),
                  subtitle: Text("Privacy, security, change number"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.chat),
                  title: Text("Chats"),
                  subtitle: Text("Theme, wallpapers, chat history"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.notifications),
                  title: Text("Notifications"),
                  subtitle: Text("Message, group & call tones"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.data_usage),
                  title: Text("Data and storage usage"),
                  subtitle: Text("Network usage, auto-download"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.help),
                  title: Text("Help"),
                  subtitle: Text("FAQ, contact us, privacy policy"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.group),
                  title: Text("Invite a friend"),
                  subtitle: Text("Share Sabay Leng with your friends"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.person_add),
                  title: Text("Contacts"),
                  subtitle: Text("Invite friends, view contacts"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.bookmark),
                  title: Text("Sabay Leng"),
                  subtitle: Text("Features, shortcuts"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.language),
                  title: Text("Language"),
                  subtitle: Text("App language"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.keyboard),
                  title: Text("Keyboard"),
                  subtitle: Text("Theme, size, layout"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.brush),
                  title: Text("Stickers"),
                  subtitle: Text("Manage stickers"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.lock),
                  title: Text("Privacy"),
                  subtitle: Text("Last seen, profile photo, about"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.security),
                  title: Text("Security"),
                  subtitle: Text("Two-step verification"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                const ListTile(
                  leading: Icon(Icons.cloud_upload),
                  title: Text("Chat backup"),
                  subtitle: Text("Auto backup, backup now"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                ListTile(
                  onTap: () {
                    AuthController().logout();
                  },
                  leading: Icon(Icons.logout),
                  title: Text("Log out"),
                  subtitle: Text(""),
                  trailing: Icon(Icons.arrow_forward_ios),
                )
              ],
            )
          )
        ),
      )
    );
  }
}
