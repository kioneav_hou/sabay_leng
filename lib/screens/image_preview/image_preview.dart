import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:sabay_leng/controllers/post_controller.dart';
import 'package:sabay_leng/controllers/user_global_controller.dart';

import '../component/comments/model_comment.dart';

class PreViewImageScreen extends StatelessWidget {
  PreViewImageScreen({Key? key, required this.index, required this.imageUrl, required this.imageUrlProfile, required this.data}) : super(key: key);
  final PostController postController = Get.put(PostController());
  final UserGlobalController userGlobalController = Get.put(UserGlobalController());
  final String imageUrl;
  final String imageUrlProfile;
  final List<dynamic>? data;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Get.back();
          },
          icon: Icon(Icons.arrow_back, color: Colors.white,),
        ),
        title: Text("Photo", style: TextStyle(color: Colors.white),),
        backgroundColor: Colors.black,
        elevation: 0,
      ),
      body: SafeArea(
        child: Stack(
          children: [
            PhotoViewGallery.builder(
              scrollPhysics: const BouncingScrollPhysics(),
              builder: (BuildContext context, int index) {
                return PhotoViewGalleryPageOptions(
                  imageProvider: NetworkImage(imageUrl.toString()),
                  minScale: PhotoViewComputedScale.contained * 0.8,
                  maxScale: PhotoViewComputedScale.covered * 2,
                  heroAttributes: PhotoViewHeroAttributes(tag: index),
                );
              },
              itemCount: 1,
              loadingBuilder: (context, event) => Center(
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                color: Colors.black.withOpacity(0.5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    postController.obx((data) =>
                        IconButton(
                          onPressed: () async {
                            data!.data![index].liked == true ? await postController.unLikePost(data!.data![index].id!) : await postController.likePost(data!.data![index].id!);
                          },
                          icon: data!.data![index].liked == true ? const Icon(Icons.favorite, color: Colors.blueAccent,) : const Icon(Icons.favorite_border, color: Colors.white,),
                        ),
                    ),
                    IconButton(
                      onPressed: (){
                        //create floating screen for comment
                        Get.to(PreviewPost(postId: data![index].id!), transition: Transition.downToUp);
                      },
                      icon: Icon(Icons.comment, color: Colors.white,),
                    ),
                    IconButton(
                      onPressed: () async {
                        await userGlobalController.comingSoon();
                      },
                      icon: Icon(Icons.share, color: Colors.white,),
                    ),
                  ],
                ),
              )
            ),
          ],
        ),
      ),
    );
  }
}
