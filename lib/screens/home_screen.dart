//create home screen
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sabay_leng/controllers/post_controller.dart';
import 'package:sabay_leng/screens/component/shimmer/shimmer_item.dart';
import 'package:sabay_leng/screens/image_preview/image_preview.dart';
import 'package:sabay_leng/services/api_references.dart';
import 'package:transparent_image/transparent_image.dart';

import 'component/header_post.dart';
import 'component/like_action.dart';
import 'component/shimmer/shimmer_home.dart';
import 'component/time_ago.dart';
import 'dart:async';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

//create state of home screen
class _HomeScreenState extends State<HomeScreen> {
  final ReferenceApi referenceApi = ReferenceApi();
  final PostController postController = Get.put(PostController());
  final ScrollController scrollController = ScrollController();
  final TimeAgo timeAgo = TimeAgo();

  @override
  void initState() {
    postController.getAllNews();
    scrollController.addListener(() {
      if(scrollController.positions.isNotEmpty){
        if(scrollController.position.pixels == scrollController.position.maxScrollExtent) {
          setState(() {
            postController.increasePaginate();
          });
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("initState ${GetStorage().read("accessToken")}");
    return Scaffold(
      //create body
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () {
            return Future.delayed(
                const Duration(seconds: 2), () =>
                postController.getAllNews()
            );
          },
          child: Column(
            children: [
              //create profile post
              HeaderPost(),
              Expanded(
                child: GetBuilder<PostController>(
                  init: PostController(),
                  builder: (controller) {
                    if(controller.isLoading.value == true){
                      return const LoadingListPage();
                    }
                    if(controller.posts! == null){
                      return const LoadingListPage();
                    }
                    //null check
                    if(controller.posts!.data == null){
                      return const LoadingListPage();
                    }else{
                      print("data: ${controller.posts!.data!.length}");
                      return ListView.builder(
                          controller: scrollController,
                          itemCount: controller.posts!.data!.length!,
                          itemBuilder: (context, index) {
                            if(controller.isLoading.value){
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            }else{
                              var data = controller.posts!.data!;
                              var imageUrl = data[index].image == null ? "https://i.stack.imgur.com/l60Hf.png" : data[index].image!.contains("http") ? data[index].image : "${referenceApi.baseUrlPost}${data[index].image}";
                              var imageUrlProfile = data[index].user!.image == null ? "https://i.stack.imgur.com/l60Hf.png" : data[index].user!.image!.contains("http") ? data[index].user?.image : "${referenceApi.baseUrlUser}${data[index].user?.image}";
                              if (data[index] != null) {
                                return Column(
                                  children: [
                                    //create story box like facebook
                                    index == 0 ? SizedBox(
                                      height: 100,
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: controller.posts!.data!.length,
                                        itemBuilder: (context, indexImg) {
                                          var story = controller.posts!.data![indexImg];
                                          var imageStory = story!.image == null ? "https://www.pngitem.com/pimgs/m/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png" : story.image!.contains("http") ? story!.image! : "${referenceApi.baseUrlPost}${story!.image!}";
                                          return Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 5),
                                            child: Column(
                                              children: [
                                                InkWell(
                                                  onTap: () {

                                                  },
                                                  child: CircleAvatar(
                                                    backgroundImage: NetworkImage(imageStory.toString()),
                                                    radius: 30,
                                                    backgroundColor: Colors.grey.shade300,
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                                                  child: Text(story!.user!.name ?? "None"),
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                    ): Container(),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        ListTile(
                                            leading: CircleAvatar(
                                              backgroundImage: NetworkImage(imageUrlProfile.toString()),
                                              backgroundColor: Colors.grey.shade300,
                                              radius: 25,
                                            ),
                                            title: Text(data[index].user!.name ?? "None"),
                                            subtitle: Text(
                                                timeAgo.timeAgoSinceDate( data[index].createdAt!)),
                                            trailing: IconButton(
                                              onPressed: () {
                                                //create menu for delete post and edit post
                                                if(data![index].owner! == false) return;
                                                showModalBottomSheet(
                                                  isScrollControlled: true,
                                                  shape: const RoundedRectangleBorder(
                                                    borderRadius: BorderRadius
                                                        .vertical(
                                                      top: Radius.circular(15),
                                                    ),
                                                  ),
                                                  context: context,
                                                  builder: (context) {
                                                    return Container(
                                                      height: MediaQuery
                                                          .of(context)
                                                          .size
                                                          .height * 0.25,
                                                      child: Column(
                                                        children: [
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment
                                                                .spaceBetween,
                                                            children: [
                                                              IconButton(
                                                                onPressed: () {
                                                                  //create close showModalBottomSheet
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                icon: const Icon(
                                                                    Icons.close),
                                                              ),
                                                              Text(controller.posts!.data![index].commentCount! > 1 ? "${controller.posts!.data![index].commentCount!} Comments" : "${controller.posts!.data![index].commentCount!} Comment"),
                                                              IconButton(
                                                                onPressed: () {},
                                                                icon: const Icon(
                                                                    Icons
                                                                        .more_horiz),
                                                              ),
                                                            ],
                                                          ),
                                                          const Divider(
                                                            thickness: 1,
                                                          ),
                                                          Expanded(
                                                            child: Column(
                                                              mainAxisAlignment: MainAxisAlignment
                                                                  .spaceEvenly,
                                                              children: [
                                                                Row(
                                                                  mainAxisAlignment: MainAxisAlignment
                                                                      .spaceEvenly,
                                                                  children: [
                                                                    Column(
                                                                      children: [
                                                                        IconButton(
                                                                          onPressed: () {
                                                                            postController.editPost(data[index].id!, context);
                                                                          },
                                                                          icon: const Icon(
                                                                              Icons
                                                                                  .edit),
                                                                        ),
                                                                        const Text(
                                                                            'Edit'),
                                                                      ],
                                                                    ),
                                                                    Column(
                                                                      children: [
                                                                        IconButton(
                                                                          onPressed: () {
                                                                            //create delete post
                                                                            Get.defaultDialog(
                                                                              title: "Delete Post",
                                                                              middleText: "Are you sure to delete this post?",
                                                                              textConfirm: "OK",
                                                                              confirmTextColor: Colors.white,
                                                                              onConfirm: (){
                                                                                controller.deletePost(data[index].id!);
                                                                                Navigator.pop(context);
                                                                              },
                                                                              textCancel: "Cancel",
                                                                              onCancel: (){
                                                                                Navigator.pop(context);
                                                                              },
                                                                            );
                                                                          },
                                                                          icon: const Icon(
                                                                              Icons
                                                                                  .delete),
                                                                        ),
                                                                        const Text(
                                                                            'Delete'),
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                );
                                              },
                                              icon: const Icon(Icons.more_horiz),
                                            )
                                        ),
                                        data[index].body != null ?
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 16.0, vertical: 5),
                                          child: Text(
                                            data[index].body ?? "",
                                            maxLines: 4,
                                            style: const TextStyle(
                                              fontSize: 16,
                                            ),
                                          ),
                                        ): Container(),
                                        InkWell(
                                          onTap: () {
                                            Get.to(() => PreViewImageScreen(index: index!, imageUrl: imageUrl!, imageUrlProfile: imageUrlProfile!, data: data));
                                          },
                                          child: FadeInImage.memoryNetwork(
                                            image: imageUrl.toString(),
                                            imageErrorBuilder: (context, error, stackTrace) {
                                              return Container(
                                                height: 200,
                                                decoration: const BoxDecoration(
                                                  color: Colors.grey,
                                                ),
                                                child: const Center(
                                                  child: Icon(Icons.error),
                                                ),
                                              );
                                            },
                                            placeholder: kTransparentImage,
                                            fit: BoxFit.contain,
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                          ),
                                        ),
                                        //create like count and comment count
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment
                                              .spaceAround,
                                          children: [
                                            //create like count
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment
                                                  .start,
                                              children: [
                                                IconButton(
                                                  onPressed: () {},
                                                  icon: const Icon(Icons.favorite),
                                                ),
                                                Text(data![index].likeCount! > 1 ? "${data![index].likeCount!} Likes" : "${data![index].likeCount!} Like"),
                                              ],
                                            ),
                                            //create comment count
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment
                                                  .start,
                                              children: [
                                                IconButton(
                                                  onPressed: () {},
                                                  icon: const Icon(Icons.comment),
                                                ),
                                                Text(data![index].commentCount! > 1 ? "${data![index].commentCount!} Comments" : "${data![index].commentCount!} Comment"),
                                              ],
                                            ),
                                          ],
                                        ),
                                        const Divider(
                                          thickness: 1,
                                        ),
                                        //create like, comment, share
                                        ActionLike(postController: postController, data: data, index: index, imageUrlProfile: imageUrlProfile),
                                      ],
                                    ),
                                    const Divider(
                                      thickness: 1,
                                    ),
                                  ],
                                );
                              }
                            }
                          }
                      );
                    }
                  }
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
