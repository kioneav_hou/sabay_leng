import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  var imgPath, imgCoverPath;

  Future<void> getLostData(int type) async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    if(image == null){
      return;
    }
    final File file = File(image.path);
    if(type == 1) {
      setState(() {
        imgPath = file;
      });
    }else{
      setState(() {
        imgCoverPath = file;
      });
    }
  }

  //create state of profile screen
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //create body
      //create cover and  profile
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Column(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Stack(
                                children: [
                                  imgCoverPath != null ?
                                    Image.file(
                                      imgCoverPath,
                                      width: MediaQuery.of(context).size.width,
                                      height: 200,
                                      fit: BoxFit.cover,
                                    )
                                   :
                                    Image.network(
                                      "https://i.pinimg.com/236x/1d/64/c1/1d64c18771601885a09d7ea69f048a1b.jpg",
                                      width: MediaQuery.of(context).size.width,
                                      height: 200,
                                      fit: BoxFit.cover,
                                    ),
                                  Positioned(
                                    bottom: 0,
                                    right: 0,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                                      child: CircleAvatar(
                                        radius: 18,
                                        backgroundColor: Colors.grey.shade400,
                                        child: IconButton(
                                          onPressed: () {
                                            getLostData(2);
                                          },
                                          icon: const Icon(Icons.edit, size: 18, color: Colors.white,),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: 50,
                            ),
                          ],
                        ),
                        Positioned(
                          bottom: 0,
                          left: 10,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(75),
                            ),
                            child: CircleAvatar(
                              radius: 55,
                              backgroundColor: Colors.white,
                              child: Stack(
                                children: [
                                  imgPath != null ?
                                    CircleAvatar(
                                      radius: 52,
                                      backgroundImage: FileImage(imgPath),
                                    )
                                 :
                                    const CircleAvatar(
                                          radius: 52,
                                          backgroundImage: NetworkImage("https://i.pinimg.com/236x/81/d0/87/81d087094d5653243df2f9ec616a4594.jpg"),
                                    ),
                                  Positioned(
                                    bottom: 0,
                                    right: 0,
                                    child: Container(
                                      width: 30,
                                      height: 30,
                                      decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: IconButton(
                                        onPressed: () {
                                          getLostData(1);
                                        },
                                        icon: const Icon(Icons.camera_alt, size: 15, color: Colors.white,),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("Sokheng", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                          Text("Alone", style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}