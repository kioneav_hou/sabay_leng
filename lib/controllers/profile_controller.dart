import 'package:get/get.dart';
import 'package:sabay_leng/models/post_model.dart';

import '../services/api.dart';

class ProfileController extends GetxController{
  RxInt counter = 1.obs;
  RxBool isLoading = false.obs;
  var posts = PostModel().obs;
  var api = Get.put(ApiService());

  @override
  void onInit() {
    isLoading(true);
  }

}