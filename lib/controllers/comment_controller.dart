import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sabay_leng/controllers/post_controller.dart';
import 'package:sabay_leng/models/comment_model.dart';
import 'package:sabay_leng/models/reply_comment_model.dart';
import 'package:sabay_leng/screens/component/loading_dialog.dart';
import '../auth/login_screen.dart';
import '../services/api_comment.dart';

class CommentController extends GetxController with StateMixin{
  final apiComment = Get.put(ApiComment());
  final PostController postController = Get.put(PostController());
  final TextEditingController comment = TextEditingController().obs();
  final DialogLoading dialogLoading = Get.put(DialogLoading());
  final FocusNode focusNode = FocusNode().obs();
  var dataReplyComment = ReplyCommentModel().obs();
  var dataCommentPost = CommentModel().obs();
  var commentId = 0.obs();
  var postId = 0.obs();
  Map dataComments = {}.obs();
  bool isSeeMore = false.obs();
  int isSeeMoreIndex = 0.obs();

  @override
  void onInit() {
    if(commentId > 0){
      getReplyComment(commentId);
    }
    if(postId > 0){
      getComment(postId);
    }
    super.onInit();
  }

  Future<void> seeMore(int index) async {
    isSeeMore = !isSeeMore;
    update();
    isSeeMoreIndex = index;
    update();
  }

  Future<void> getComment (postId) async {
    print("postId: $postId");
    try{
      final res = await apiComment.getComment(postId: postId);
      res.fold((left) {
        print("error: $left");
        change(null, status: RxStatus.error(left.toString()));
      }, (right) {
        print("right: $right");
        dataCommentPost = right;
        change(dataCommentPost, status: RxStatus.success());
        print("dataComments: ${dataCommentPost.data!.length}");
      });
    } catch (e) {
      print("error: $e");
      change(null, status: RxStatus.error(e.toString()));
    }
  }

  Future<void> getReplyComment (commentId) async {
    try{
      final res = await apiComment.getReplyComment(commentId: commentId);
      res.fold((left) {
        print("error: $left");
      }, (right) {
        print("right: $right");
        dataReplyComment = right;
        change(dataReplyComment, status: RxStatus.success());
        print("dataReplyComment: ${dataReplyComment.data!.length}");
      });
    } catch (e) {
      print("error: $e");
    }
  }


  Future<void> postComment(id, comment) async {
    try {
      dialogLoading.dialogLoading();
      var res = await apiComment.postComment(postId: id, comment: comment);
      res.fold((left) {
        print("left: $left");
        dialogLoading.dialogDismiss();
        if(left == "Token Expired"){
          Get.offAll(() => LoginScreen());
        }
      } , (right){
        print("right: $right");
        dialogLoading.dialogDismiss();
        //close keyboard
        FocusScope.of(Get.context!).unfocus();
        getComment(id);
        clearComment();
      });
    } catch (e) {
      print("error get: ${e.toString()} ");
      dialogLoading.dialogDismiss();
    }
  }

  Future<void> editComment(id, context) async {
    try{
      final res = await apiComment.editComment(id: id);
      res.fold((left) {
        print("error: $left");
        Get.defaultDialog(
          title: "Error",
          middleText: left.toString(),
          textConfirm: "OK",
          onConfirm: () => Get.back(),
        );
      }, (right) {
        print("right: $right");
        dataComments = right;
        comment.text = right["text"].toString();
        //open keyboard comment
        FocusScope.of(context).requestFocus(FocusNode());
        Navigator.pop(context);
        print("comment: $comment");
      });
    } catch (e) {
      print("error: $e");
    }
  }

  Future<void> updateComment (id, comment) async {
    try{
      final res = await apiComment.updateComment(comment: comment, id: id.toInt());
      res.fold((left) {
        Get.defaultDialog(
          title: "Error",
          middleText: left.toString(),
          textConfirm: "OK",
          onConfirm: () => Get.back(),
        );
      }, (right) {
        print("right: $right");
        getComment(postId);
        clearComment();
        FocusScope.of(Get.context!).unfocus();
      });
    } catch (e) {
      print("error: $e");
      Get.defaultDialog(
        title: "Error",
        middleText: e.toString(),
        textConfirm: "OK",
        onConfirm: () => Get.back(),
      );
    }
  }

  Future<void> deleteComment (id) async {
    try{
      final res = await apiComment.deleteComment(id: id.toInt());
      res.fold((left) {
        Get.defaultDialog(
          title: "Error",
          middleText: left.toString(),
          textConfirm: "OK",
          onConfirm: () => Get.back(),
        );
      }, (right) {
        print("right: $right");
        getComment(postId);
        Get.back();
      });
    } catch (e) {
      print("error: $e");
      Get.defaultDialog(
        title: "Error",
        middleText: e.toString(),
        textConfirm: "OK",
        onConfirm: () => Get.back(),
      );
    }
  }

  void clearComment(){
    dataComments.clear();
    comment.clear();
  }

  Future<void> focusReplyCommentFiled() async {
    focusNode.requestFocus();
  }

  Future<void> replyComment (id, replyText, context) async {
    try{
      final res = await apiComment.replyComment(commentId: id.toInt(), replyText: replyText);
      res.fold((left) {
        Get.defaultDialog(
          title: "Error",
          middleText: left.toString(),
          textConfirm: "OK",
          onConfirm: () => Get.back(),
        );
      }, (right) {
        print("right: $right");
        postController.getAllNews();
        getReplyComment(commentId);
        clearComment();
        FocusScope.of(context).unfocus();
      });
    } catch (e) {
      print("error: $e");
      Get.defaultDialog(
        title: "Error",
        middleText: e.toString(),
        textConfirm: "OK",
        onConfirm: () => Get.back(),
      );
    }
  }
}