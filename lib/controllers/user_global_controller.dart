import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

class UserGlobalController {
  final _storage = GetStorage();

  Future<void> setCurrentUser({required Map<String, dynamic> user}) async {
    await _storage.write("currentUser", user);
  }

  Future<void> clearCurrentUser() async {
    await _storage.remove("currentUser");
  }

  Future<Map<String, dynamic>> getCurrentUser() async {
    final user = await _storage.read("currentUser");
    return user;
  }

  Future<void> setTokenUser({required String token}) async {
    await _storage.write("accessToken", token);
  }

  Future<void> revokeTokenUser() async {
    await _storage.remove("accessToken");
  }

  Future<dynamic> getTokenUser() async {
    final token = await _storage.read("accessToken");
    return token;
  }

  Future<void> setTokenExpiresIn({required int tokenExpiresIn}) async {
    await _storage.write("token_expires_in", tokenExpiresIn);
  }

  Future<void> clearTokenExpiresIn() async {
    await _storage.remove("token_expires_in");
  }

  Future<bool> isOwner(int resourceId) async {
    final user = await getCurrentUser();
    if (resourceId == user["id"]) {
      return true;
    } else {
      // The current user is not the owner of the resource
      return false;
    }
  }

  Future<void> comingSoon () async {
    await Get.defaultDialog(
      title: "Coming Soon",
      middleText: "This feature is coming soon!",
      textConfirm: null,
      barrierDismissible: true,
    );
  }

}