import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sabay_leng/controllers/user_global_controller.dart';

import '../auth/login_screen.dart';
import '../bottom_navigation/bottom_screen.dart';
import '../screens/component/loading_dialog.dart';
import '../services/api_auth.dart';

class AuthController extends GetxController with StateMixin{

  RxBool isLoading = false.obs;
  RxString otp = "".obs;
  RxBool isLogin = false.obs;
  var api = Get.put(AuthApi());
  RxString registerName = "".obs;
  final DialogLoading dialogLoading = Get.put(DialogLoading());
  final UserGlobalController userGlobalController = Get.put(UserGlobalController());

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> confirmOTRegister (
      name,
      phone,
      countryCode,
      email,
      password,
      confirmPassword
      ) async {
    dialogLoading.dialogLoading();
    FirebaseAuth auth = FirebaseAuth.instance;
    auth.setSettings(forceRecaptchaFlow: true);
    await auth.verifyPhoneNumber(
      phoneNumber: '${countryCode+phone}',
      verificationCompleted: (PhoneAuthCredential credential) {
        dialogLoading.dialogDismiss();
      },
      verificationFailed: (FirebaseAuthException e) {
        dialogLoading.dialogDismiss();
        Get.snackbar("OTP", e.toString(), snackPosition: SnackPosition.TOP);
      },
      codeSent: (String verificationId, int? resendToken) {
        dialogLoading.dialogDismiss();
        Get.defaultDialog(
          title: "OTP",
          middleText: "Enter OTP",
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                autofillHints: const [AutofillHints.oneTimeCode],
                onChanged: (value) {
                  otp.value = value;
                },
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () async {
                PhoneAuthCredential credential = PhoneAuthProvider.credential(verificationId: verificationId, smsCode: otp.value);
                var result = await auth.signInWithCredential(credential);
                if(result.user != null) {
                  print("success");
                  register(
                    name,
                    phone,
                    email,
                    password,
                    confirmPassword
                  );
                } else {
                  print("fail");
                }
              },
              child: const Text("Confirm"),
            ),
          ],
        );
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        dialogLoading.dialogDismiss();
      },
    );
  }

  @override
  void register(
    name,
    phone,
    email,
    password,
    confirmPassword
  ) async {
    try{
      dialogLoading.dialogLoading();
      isLoading(true);
      var res = await AuthApi().register(
        name: name,
        phone: phone,
        email: email,
        password: password,
        confirmPassword: confirmPassword
      );
      res.fold((left) {
        print("left: $left");
        dialogLoading.dialogDismiss();
        Get.snackbar("Register",
          left.toString(),
          snackPosition: SnackPosition.TOP,
          animationDuration: const Duration(microseconds: 0),
          duration: const Duration(seconds: 4),
          margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 3),
          borderRadius: 10,
          isDismissible: true,
        );
      } , (right) async {
        dialogLoading.dialogDismiss();
        await userGlobalController.setTokenUser(token: right["access_token"]);
        await userGlobalController.setCurrentUser(user: right["user"]);
        await userGlobalController.setTokenExpiresIn(tokenExpiresIn: right["expires_in"]);
        Get.snackbar("Register", right["message"], snackPosition: SnackPosition.TOP);
        Get.offAll(() => const BottomNavigationScreen());
      });
      isLoading(false);
      update();
    } catch (e) {
      //debug error
      print("error: $e");
      dialogLoading.dialogDismiss();
      Get.snackbar("Register", "Some things went wrong!", snackPosition: SnackPosition.TOP);
    }
  }

  void login(
    email,
    password
  ) async {
    try{
      dialogLoading.dialogLoading();
      isLoading(true);
      var res = await AuthApi().login(
        email: email,
        password: password
      );
      res.fold((left) {
        print("left: $left");
        dialogLoading.dialogDismiss();
        Get.defaultDialog(
          title: "Login",
          middleText: left.toString(),
          textConfirm: "OK",
          confirmTextColor: Colors.white,
          onConfirm: () {
            Get.back();
          },
        );
      } , (right) async {
        print("right: $right");
        await userGlobalController.revokeTokenUser();
        await userGlobalController.setTokenUser(token: right["access_token"]);
        await userGlobalController.setCurrentUser(user: right["user"]);
        await userGlobalController.setTokenExpiresIn(tokenExpiresIn: right["expires_in"]);
        isLogin(true);
        dialogLoading.dialogDismiss();
        Get.snackbar("Login", "Login successfully!", snackPosition: SnackPosition.TOP);
        Get.offAll(() => const BottomNavigationScreen());
      });
      isLoading(false);
      update();
    } catch (e) {
      print("errorn hh: ${e.toString()}"); //debug error
      dialogLoading.dialogDismiss();
      Get.defaultDialog(
        title: "Login",
        middleText: "Some things went wrong!",
        textConfirm: "OK",
        confirmTextColor: Colors.white,
        onConfirm: () {
          Get.back();
        },
      );
    }
  }

  //create logout method clear only storage
  void logout() {
    Get.defaultDialog(
      title: "Logout",
      middleText: "Are you sure you want to logout?",
      textConfirm: "Yes",
      confirmTextColor: Colors.white,
      textCancel: "No",
      onCancel: () {
        Get.back();
      },
      onConfirm: () async {
        await userGlobalController.revokeTokenUser();
        final token = await userGlobalController.getTokenUser();
        Get.snackbar("Logout", "Logout successfully!", snackPosition: SnackPosition.TOP);
        if(token == null) {
          isLogin(false);
          Get.offAll(LoginScreen());
        }
      },
    );
  }
}