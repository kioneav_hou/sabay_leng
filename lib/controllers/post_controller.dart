import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sabay_leng/auth/login_screen.dart';
import 'package:sabay_leng/models/edit_post_model.dart';
import 'package:sabay_leng/models/post_model.dart';
import 'dart:io';

import '../screens/component/loading_dialog.dart';
import '../screens/component/upload_post_screen.dart';
import '../services/api.dart';
import '../services/api_references.dart';

class PostController extends GetxController with StateMixin<PostModel> {
  RxInt pagination = 5.obs;
  RxBool isLoading = false.obs;
  var posts = PostModel();
  var editPosts = EditPost().obs;
  var api = Get.put(ApiService());
  var referenceApi = Get.put(ReferenceApi());
  //create variable for store image path
  var imagePath;
  final DialogLoading dialogLoading = Get.put(DialogLoading());

  @override
  void onInit() async {
    print("data: ${posts.data}" );
    isLoading(true);
    await getAllNews();
    super.onInit();
  }

  void increasePaginate()  {
    pagination.value += 3;
    Future.delayed(const Duration(seconds: 1));
    getAllNews();
  }

  //create function to get data from api with future and either
  Future<void> getAllNews () async {
    try {
      final res = await api.getAllNews(pagination: pagination.value);
      res.fold((left) {
        print("left: $left");
        isLoading(false);
        if(left == "Token Expired"){
          Get.offAll(() => LoginScreen());
        }
      } , (right){
        print("right: $right");
        isLoading(false);
        posts = right;
        change(posts, status: RxStatus.success());
        print("posts: ${posts!.data!.length!}");
      });
    } catch (e) {
      print("error get: ${e.toString()} ");
      isLoading(false);
      change(null, status: RxStatus.error(e.toString()));
    }
  }

  //create function post image api
  Future<void> postImage(body) async {
    try {
      dialogLoading.dialogLoading();
      var res = await api.postImage(imagePath: imagePath, body: body);
      res.fold((left) {
        print("left: $left");
        dialogLoading.dialogDismiss();
        Get.defaultDialog(
          title: "Post",
          middleText: left.toString(),
          textConfirm: "OK",
          confirmTextColor: Colors.white,
          onConfirm: () => Get.back(),
        );
        if(left.toString() == "Token Expired"){
          Get.offAll(() => LoginScreen());
        }
      } , (right){
        print("right: $right");
        dialogLoading.dialogDismiss();
        pagination.value = 5;
        imagePath = null;
        change(imagePath, status: RxStatus.loading());
        getAllNews();
        Get.back();
      });
      update();
    } catch (e) {
      dialogLoading.dialogDismiss();
      Get.defaultDialog(
        title: "Something went wrong",
        middleText: e.toString(),
        textConfirm: "OK",
        confirmTextColor: Colors.white,
        onConfirm: () => Get.back(),
      );
    }
  }

  Future<void> deletePost (id) async {
    try{
      final res = await api.deletePost(id: id);
      res.fold((left) {
        print("left: $left");
        Get.back();
        Get.snackbar(
          "Delete Post",
          left.toString(),
          snackPosition: SnackPosition.TOP,
          animationDuration: const Duration(microseconds: 0),
          duration: const Duration(seconds: 4),
          margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 3),
          borderRadius: 10,
          isDismissible: true,
        );
        if(left == "Token Expired"){
          Get.offAll(() => LoginScreen());
        }
      }, (right) {
        print("right: $right");
        getAllNews();
        Get.back();
      });
    } catch (e) {
      print("error: $e");
      Get.back();
      Get.defaultDialog(
        title: "Something went wrong",
        middleText: e.toString(),
        textConfirm: "OK",
        confirmTextColor: Colors.white,
        onConfirm: () => Get.back(),
      );
    }
  }

  Future<void> editPost (id, context) async {
    try{
      dialogLoading.dialogLoading();
      final res = await api.editPost(id: id.toInt());
      res.fold((left) {
        print("left: $left");
        dialogLoading.dialogDismiss();
        Get.defaultDialog(
          title: "Edit Post",
          middleText: left.toString(),
          textConfirm: "OK",
          confirmTextColor: Colors.white,
          onConfirm: () => Get.back(),
        );
        if(left == "Token Expired"){
          Get.offAll(() => LoginScreen());
        }
      }, (right) {
        print("right: $right");
        dialogLoading.dialogDismiss();
        var imageUrl = right.image!.contains("http") ? right.image : "${referenceApi.baseUrlPost}${right.image}";
        right.image = imageUrl;
        editPosts(right);
        Navigator.pop(context);
        if(editPosts.value.image != null){
          Get.to(() => const UploadPostScreen());
        }
      });
    } catch (e) {
      print("error: $e");
      dialogLoading.dialogDismiss();
      Get.defaultDialog(
        title: "Edit Post",
        middleText: e.toString(),
        textConfirm: "OK",
        confirmTextColor: Colors.white,
        onConfirm: () => Get.back(),
      );
    }
  }

  Future<void> updatePost (body) async {
    try{
      dialogLoading.dialogLoading();
      var res = await api.updatePost(body: body, imagePath: imagePath, id: editPosts!.value!.id!);
      res.fold((left) {
        print("left: $left");
        dialogLoading.dialogDismiss();
        Get.snackbar("Update Post",
          left.toString(),
          snackPosition: SnackPosition.TOP,
          animationDuration: const Duration(microseconds: 0),
          duration: const Duration(seconds: 4),
          margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 3),
          borderRadius: 10,
          isDismissible: true,
        );
        if(left == "Token Expired"){
          Get.offAll(() => LoginScreen());
        }
      }, (right) {
        print("right: $right");
        dialogLoading.dialogDismiss();
        pagination.value = 5;
        getAllNews();
        Get.back();
      });
    } catch (e) {
      print("error: $e");
      dialogLoading.dialogDismiss();
      Get.defaultDialog(
        title: "Update Post",
        middleText: e.toString(),
        textConfirm: "OK",
        confirmTextColor: Colors.white,
        onConfirm: () => Get.back(),
      );
    }
  }

  Future<void> likePost(id) async {
    try{
      final res = await api.likePost(id: id);
      res.fold((left) {
        print("left: $left");
        if(left == "Token Expired"){
          Get.offAll(() => LoginScreen());
        }
      }, (right) {
        print("right: $right");
        getAllNews();
      });
    } catch (e) {
      print("error: $e");
    }
  }

  Future<void> unLikePost(id) async {
    try{
      final res = await api.unLikePost(id: id);
      res.fold((left) {
        print("left: $left");
        if(left == "Token Expired"){
          Get.offAll(() => LoginScreen());
        }
      }, (right) {
        print("right: $right");
        getAllNews();
      });
    } catch (e) {
      print("error: $e");
    }
  }

  Future<void> getLostData() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    if(image == null){
      return;
    }
    editPosts.value.image = null;
    final File file = File(image.path);
    imagePath = file;
    update();
  }

  //create function cancel image post with future
  Future<void> cancelImagePost() async {
    imagePath = null;
    update();
  }

  Future<void> cancelImageEditPost() async {
    editPosts.value.image = null;
  }
}