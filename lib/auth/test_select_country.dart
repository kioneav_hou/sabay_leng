import 'package:flutter/material.dart';
import 'package:country_selector_widget/country_selector.dart';
import 'package:get/get.dart';

class AppScreen extends StatelessWidget {
  const AppScreen({super.key, required this.title});
  final String title;

  Future<void> showSuccessDialog(
      BuildContext context,
      Country country,
      ) async {
    await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text(
            '[onSelectedCountry]',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text('${country.toJson()}'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text(
                'Close',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: GestureDetector(
          onTap: () async {
            await showCountrySelectorBottomSheet(
              context: context,
              // enable dialCode instead of selected circle
              withDialCode: true,
              customAppBar: AppBar(
                title: null,
                backgroundColor: Colors.white,
                elevation: 0.0,
                leading: IconButton(
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                  onPressed: () => Navigator.pop(context),
                ),
              ),
              showSelectedWidget: false,
              bottomSheetHeight: MediaQuery.of(context).size.height * 0.95,
              // set the default selected country
              refCountryCode: "kh",
              withoutMatchText: "No country found",
              onSelectedCountry: (country) async {
                // dialog to show the info of the country object
                await showSuccessDialog(
                  context,
                  country,
                );
              },
            );
          },
          child: Center(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 10.0),
              padding: const EdgeInsets.symmetric(
                vertical: 10.0,
                horizontal: 10.5,
              ),
              color: Colors.grey.shade200,
              child: const Text(
                "showBottomSheet",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}