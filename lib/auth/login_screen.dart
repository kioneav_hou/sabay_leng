import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:sabay_leng/auth/register_screen.dart';

import '../controllers/auth_controller.dart';
import 'package:sabay_leng/auth/confirm_otp.dart';

class LoginScreen extends StatefulWidget{
  LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}
class _LoginScreenState extends State<LoginScreen>{
  bool _isVisibility = false;
  String themeBgColor = "white";
  // final String imageUrl = "http://10.0.2.2:8000/image/"; //emulator
  final String imageUrl = "http://192.168.88.64:8000/image/"; //wifi device

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final AuthController authController = Get.put(AuthController());
  final _formKey = GlobalKey<FormState>();

  @override
  initState() {
    FlutterNativeSplash.remove();
    super.initState();
  }

  @override
  void dispose() {
    authController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: themeBgColor == "white" ? Colors.white : Colors.black,
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(child: Column()),
                  Expanded(child: Column()),
                  const Text(
                    "Sign In",
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.email),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      labelText: 'Email',
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your email';
                      }
                      return null;
                    },
                    onChanged: (value) {

                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    controller: _passwordController,
                    obscureText: !_isVisibility,
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.lock),
                      suffixIcon: InkWell(
                        onTap: () {
                          setState(() {
                            _isVisibility = !_isVisibility;
                          });
                        },
                        child: _isVisibility ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      labelText: 'Password',
                    ),
                    validator: (value) {
                      if(value!.isEmpty) {
                        return "Please enter your password";
                      }else{
                        return null;
                      }
                    }
                  ),
                  //create button login
                  const SizedBox(
                    height: 15,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Container(
                      height: 50,
                      child: ElevatedButton(
                        onPressed: () {
                          // Get.changeTheme(
                          //   Get.isDarkMode ? ThemeData.light() : ThemeData.dark(),
                          // );
                          // setState(() {
                          //   themeBgColor = Get.isDarkMode ? "white" : "black";
                          // });
                          if(_formKey.currentState!.validate()) {
                            authController.login(
                              _emailController.text,
                              _passwordController.text
                            );
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          primary: themeBgColor == "white" ? Colors.black : Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                        ),
                        child: Text('Login'.toUpperCase(),
                          style: Theme.of(context).textTheme.button!.copyWith(
                            color: themeBgColor == "white" ? Colors.white : Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Column(
                    children: [
                      TextButton(
                        onPressed: () {
                          Get.to(() => const ConfirmOtpScreen());
                        },
                        child: const Text("Forgot Password?"),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text("Don't have an account?"),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: InkWell(
                              onTap: () {
                                Get.to(const RegisterScreen() );
                              },
                              child: const Text("Register", style: TextStyle(
                                color: Colors.blue,
                              ),),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  //create socail login
                  Expanded(child: Column()),
                  const Text("Or login with"),
                  Expanded(child: Column()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 50,
                        height: 50,
                        child: ElevatedButton(
                          onPressed: () {

                          },
                          style: ElevatedButton.styleFrom(
                            shape: const CircleBorder(),
                            backgroundColor: Colors.white,
                            padding: const EdgeInsets.all(0),
                          ),
                          child: Image.asset(
                          "images/facebook.jpg",
                            fit: BoxFit.cover,
                            width: 50,
                            height: 50,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      SizedBox(
                        width: 50,
                        height: 50,
                        child: ElevatedButton(
                          onPressed: () {

                          },
                          style: ElevatedButton.styleFrom(
                            shape: const CircleBorder(),
                            backgroundColor: Colors.white,
                            padding: const EdgeInsets.all(0),
                          ),
                          child: Image.asset(
                            "images/google.png",
                            fit: BoxFit.cover,
                            width: 50,
                            height: 50,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      )
    );
  }
}
