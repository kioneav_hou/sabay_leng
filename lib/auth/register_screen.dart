//create screen register with stateful widget and state mixin
import 'package:country_selector_widget/country_selector.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sabay_leng/controllers/auth_controller.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool _isVisibility = false;
  bool _isVisibilityCon = false;
  final AuthController authController = Get.put(AuthController());
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();
  final TextEditingController countryCode = TextEditingController(text: "+1");
  final _formkey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _nameController.dispose();
    _confirmPasswordController.dispose();
    authController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 1,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Form(
              key: _formkey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                       "Sign Up",
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.person),
                        hintText: "Name",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                      validator: (value){
                        if(value!.isEmpty){
                          return "Please enter your name";
                        }else{
                          return null;
                        }
                      }
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                        controller: _phoneNumberController,
                        decoration: InputDecoration(
                          prefixIcon: InkWell(
                            onTap: () async {
                              await showCountrySelectorBottomSheet(
                                context: context,
                                // enable dialCode instead of selected circle
                                withDialCode: true,
                                customAppBar: AppBar(
                                  title: null,
                                  backgroundColor: Colors.white,
                                  elevation: 0.0,
                                  leading: IconButton(
                                    icon: const Icon(
                                      Icons.arrow_back,
                                      color: Colors.black,
                                    ),
                                    onPressed: () => Navigator.pop(context),
                                  ),
                                ),
                                showSelectedWidget: false,
                                bottomSheetHeight: MediaQuery.of(context).size.height * 0.95,
                                // set the default selected country
                                refCountryCode: "kh",
                                withoutMatchText: "No country found",
                                onSelectedCountry: (country) async {
                                  // dialog to show the info of the country object
                                  setState(() {
                                    countryCode.text = country.dialCode!;
                                  });
                                },
                              );
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: 80.0,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(countryCode.text),
                                      //create ruler for space
                                      const SizedBox(width: 1.0),
                                      const Icon(Icons.arrow_drop_down),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          hintText: "Phone Number",
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                        ),
                        validator: (value){
                          if(value!.isEmpty){
                            return "Please enter your phone number";
                          }else{
                            return null;
                          }
                        }
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _emailController,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.email),
                        hintText: "Email",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                      validator: (value) {
                        if(value!.isEmpty){
                          return "Please enter your email";
                        }
                        if(!GetUtils.isEmail(value!)){
                          return "Please enter valid email";
                        }else{
                          return null;
                        }
                      },
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _passwordController,
                      obscureText: !_isVisibility,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.lock),
                        hintText: "Password",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              _isVisibility = !_isVisibility;
                            });
                          },
                          icon: Icon(
                            _isVisibility ? Icons.visibility : Icons.visibility_off,
                          ),
                        ),
                      ),
                      validator: (value) {
                        if(!GetUtils.hasMatch(value, r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$')){
                          return "- Password must be at least 6 characters.\n- 1 uppercase, 1 lowercase.\n- 1 number.";
                        }
                        else{
                          return null;
                        }
                      },
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _confirmPasswordController,
                      obscureText: !_isVisibilityCon,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.lock),
                        hintText: "Confirm Password",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              _isVisibilityCon = !_isVisibilityCon;
                            });
                          },
                          icon: Icon(
                            _isVisibilityCon ? Icons.visibility : Icons.visibility_off,
                          ),
                        ),
                      ),
                      validator: (value){
                        if(value!.isEmpty){
                          return "Please enter your confirm password";
                        }
                        if(value != _passwordController.text){
                          return "Confirm password not match";
                        }
                      }
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        if(_formkey.currentState!.validate()){
                         await authController.confirmOTRegister(
                            _nameController.text,
                            _phoneNumberController.text,
                            countryCode.text,
                            _emailController.text,
                            _passwordController.text,
                            _confirmPasswordController.text,
                          );
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        backgroundColor: Colors.black,
                      ),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        alignment: Alignment.center,
                        child: Text("Register".toUpperCase())
                      ),
                    ),
                    //create text to link to login screen
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text("Already have an account?"),
                        TextButton(
                          onPressed: () {
                            Get.back();
                          },
                          child: const Text("Login"),
                        ),
                      ]
                    ),
                  ],
                ),
            ),
            ),
          ),
      ),
    );
  }
}

