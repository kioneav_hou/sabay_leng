import 'package:country_selector_widget/country_selector.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:sabay_leng/auth/login_screen.dart';
import 'package:get/get.dart';
import 'package:sabay_leng/screens/component/loading_dialog.dart';

class ConfirmOtpScreen extends StatefulWidget {
  const ConfirmOtpScreen({Key? key}) : super(key: key);

  @override
  State<ConfirmOtpScreen> createState() => _ConfirmOtpScreenState();
}

class _ConfirmOtpScreenState extends State<ConfirmOtpScreen> {
  TextEditingController phoneNumber = TextEditingController();
  TextEditingController countryCode = TextEditingController(text: "+1");
  TextEditingController otp = TextEditingController();
  final DialogLoading dialogLoading = Get.put(DialogLoading());
  final _formKey = GlobalKey<FormState>();

  Future<void> verifyPhoneNumber() async {
    dialogLoading.dialogLoading();
    FirebaseAuth auth = FirebaseAuth.instance;
    await auth.verifyPhoneNumber(
      phoneNumber: '${countryCode.text}${phoneNumber.text}',
      verificationCompleted: (PhoneAuthCredential credential) {
        dialogLoading.dialogDismiss();
      },
      verificationFailed: (FirebaseAuthException e) {
        dialogLoading.dialogDismiss();
      },
      codeSent: (String verificationId, int? resendToken) {
        dialogLoading.dialogDismiss();
        showDialog(
          barrierDismissible: true,
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text("Enter OTP"),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: otp,
                    autofillHints: const [AutofillHints.oneTimeCode],
                  ),
                ],
              ),
              actions: [
                TextButton(
                  onPressed: () async {
                    dialogLoading.dialogLoading();
                    PhoneAuthCredential credential = PhoneAuthProvider.credential(verificationId: verificationId, smsCode: otp.text);
                    var result = await auth.signInWithCredential(credential);
                    if(result.user != null) {
                      print("success");
                      dialogLoading.dialogDismiss();
                      Get.offAll(() => LoginScreen());
                    } else {
                      dialogLoading.dialogDismiss();
                      print("fail");
                    }
                  },
                  child: const Text("Confirm"),
                ),
              ],
            );
          },
        );
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        dialogLoading.dialogDismiss();
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
          child: Column(
            children: [
              Row(
                children: [
                  IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: const Icon(Icons.arrow_back),
                  ),
                  const Expanded(
                    child: Text(
                      "Forgot Password",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              Row(
                children: [
                  Expanded(
                    child: Form(
                      key: _formKey,
                      child: TextFormField(
                        controller: phoneNumber,
                        decoration: InputDecoration(
                          // prefixIcon: const Icon(Icons.call),
                          prefixIcon: InkWell(
                            onTap: () async {
                              await showCountrySelectorBottomSheet(
                                context: context,
                                // enable dialCode instead of selected circle
                                withDialCode: true,
                                customAppBar: AppBar(
                                  title: null,
                                  backgroundColor: Colors.white,
                                  elevation: 0.0,
                                  leading: IconButton(
                                    icon: const Icon(
                                      Icons.arrow_back,
                                      color: Colors.black,
                                    ),
                                    onPressed: () => Navigator.pop(context),
                                  ),
                                ),
                                showSelectedWidget: false,
                                bottomSheetHeight: MediaQuery.of(context).size.height * 0.95,
                                // set the default selected country
                                refCountryCode: "kh",
                                withoutMatchText: "No country found",
                                onSelectedCountry: (country) async {
                                  // dialog to show the info of the country object
                                  setState(() {
                                    countryCode.text = country.dialCode!;
                                  });
                                },
                              );
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: 80.0,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(countryCode.text),
                                      //create ruler for space
                                      const SizedBox(width: 1.0),
                                      const Icon(Icons.arrow_drop_down),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          hintText: "Phone Number",
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                        ),
                        validator: (value) {
                          if(value!.isEmpty) {
                            return "Please enter phone number";
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue),
                  side: MaterialStateProperty.all(const BorderSide(color: Colors.blue)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                  ),
                ),
                onPressed: () async {
                  if(_formKey.currentState!.validate()) {
                    await verifyPhoneNumber();
                  }
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50.0,
                  child: Center(child: const Text("Confirm"))
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
