import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';

class ReferenceApi {
  final _storage = GetStorage();
  late final String token = _storage.read("accessToken");
  // final String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTkyLjE2OC44OC42NDo4MDAwL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNjkyOTMwMDg5LCJleHAiOjE2OTI5MzM2ODksIm5iZiI6MTY5MjkzMDA4OSwianRpIjoiUVBMTDRUYndaYzZsVlJxeiIsInN1YiI6IjEiLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.mhQDAaaXGY3OBzqUm51vOdbWqEp16oe_YGyPj5-An2M";
  // final String baseUrl = "http://10.0.2.2:8000/api"; //url emulator
  final String baseUrl = "http://192.168.88.64:8000/api"; //url wifi device
  final String baseUrlPost = "http://192.168.88.64:8000/posts/";
  final String baseUrlUser = "http://192.168.88.64:8000/image/";
}
