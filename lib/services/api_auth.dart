import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:sabay_leng/models/failed_model.dart';
import 'package:sabay_leng/services/api_references.dart';


class AuthApi {
  final ReferenceApi referenceApi = ReferenceApi();
  late String registerUrl = "${referenceApi.baseUrl}/auth/register";
  late String loginUrl = "${referenceApi.baseUrl}/auth/login";
  late String refreshTokenUrl = "${referenceApi.baseUrl}/auth/refresh/token";
  late String logoutUrl = "${referenceApi.baseUrl}/auth/logout";
  late final String token = referenceApi.token;
  final _dio = Dio();

  Future<Either<String, dynamic>> register({
    required String name,
    required String phone,
    required String email,
    required String password,
    required String confirmPassword
  }) async {
    try {
      final response = await _dio.post(
          registerUrl,
          data: {
            "name": name,
            "phone": phone,
            "email": email,
            "password": password,
            "password_confirmation": confirmPassword
          },
          options: Options(
              headers: {
                "Accept": "application/json",
              }
          )
      );
      final data = response.data;
      print("data: ${data}");
      if (response.statusCode == 201) {
        return Right(data);
      }
      return Left("Can't register");
    } on DioError catch (e) {
      print("error : ${e}");
      final data = FailedModel.fromJson(jsonDecode(e.response!.toString()));
      print("error : $data");
      return Left(data!.failed![0].toString());
    }
  }

  Future<Either<String, dynamic>> login({
    required String email,
    required String password
  }) async {
    try {
      final response = await _dio.post(
          loginUrl,
          data: {
            "email": email,
            "password": password,
          },
          options: Options(
              headers: {
                "Accept": "application/json",
              }
          )
      );
      final data = response.data;
      print("data: ${response}");
      if (response.statusCode == 200) {
        return Right(data);
      }
      return Left("Can't login");
    } on DioError catch (e) {
      print("error bbb: ${e}");
      if (e.response == null) {
        return Left("Can't connect to server!");
      }
      if (e.response!.statusCode == 401 || e.response!.statusCode == 422) {
        return Left("Email or password is incorrect!");
      }
      final data = FailedModel.fromJson(jsonDecode(e.response!.toString()));
      return Left(data!.failed![0].toString());
    }
  }

  //cretae function to refresh token
  Future<Either<String, String>> refreshToken () async {
    try {
      final response = await _dio.post(
          refreshTokenUrl,
          options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer $token"
              }
          )
      );
      final data = response.data;
      if (response.statusCode == 200) {
        return Right(data["access_token"]);
      }
      return Left("Can't refresh token!");
    } catch (e) {
      return Left("Can't refresh token!");
    }
  }
}