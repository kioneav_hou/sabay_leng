import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:sabay_leng/controllers/user_global_controller.dart';
import 'package:sabay_leng/models/post_model.dart';
import 'package:sabay_leng/services/api_references.dart';

import '../models/edit_post_model.dart';
import '../models/failed_model.dart';

class ApiService {
  final ReferenceApi referenceApi = ReferenceApi();
  final UserGlobalController userGlobalController = UserGlobalController();
  late String token = referenceApi.token;
  late String testUrl = "${referenceApi.baseUrl}/test";
  late String getPostUrl = "${referenceApi.baseUrl}/get/posts";
  late String postUrl = "${referenceApi.baseUrl}/posts";
  late String deleteUrl = "${referenceApi.baseUrl}/delete/posts";
  late String editUrl = "${referenceApi.baseUrl}/show/posts";
  late String updateUrl = "${referenceApi.baseUrl}/update/posts";
  late String likeUrl = "${referenceApi.baseUrl}/likes/post";
  late String unlikeUrl = "${referenceApi.baseUrl}/unlikes/post";

  final _dio = Dio();

  //create function to get data from api with future and either

  Future<Either<String, PostModel>> getAllNews ({int? pagination}) async {
    print("pagination: $pagination");
    try {
      final response = await _dio.get(
        "$getPostUrl?paginate=$pagination",
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      final data = response!.data!;
      if(response.statusCode == 200){
        return Right(PostModel.fromJson(data));
      }
      return Left("Error");
    } on DioError catch (e) {
      print("error: $e");
      if(e!.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      return Left(e.response.toString());
    }
  }

  Future<Either<String, String>> postImage ({String? body, required File imagePath}) async {
    print("image: ${imagePath}");
    try{
      final response = await _dio.post(
          postUrl,
          data: FormData.fromMap({
            'body': body,
            'image': await MultipartFile.fromFile(imagePath.path)
          }),
          options: Options(
              headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json',
                "Authorization": "Bearer $token"
              }
          )
      );
      print("response: ${response.data}");
      final data = response.data;
      print("data: ${response}");
      if(response.statusCode == 200){
        return const Right("Post Success");
      }
      return const Left("Error");
    } on DioError catch (e) {
      print("error: ${e.response}");
      if(e!.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      print("error: ${e.response!} hhh");
      return Left("${e.response!.data[0]} \n (Allow only image file and size less than 2MB)");
    }
  }


  Future<Either<String, String>> deletePost ({required int id}) async {
    try{
      final response = await _dio.post(
        "$deleteUrl/$id",
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      final data = response.data;
      if(response.statusCode == 200){
        return const Right("Delete Success");
      }
      return const Left("Can't delete post");
    } on DioError catch (e) {
      print("error: $e");
      if(e!.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      return Left(e.response!.data!["message"].toString());
    }
  }

  Future<Either<String, EditPost>> editPost ({required int id}) async {
    try{
      final response = await _dio.get(
        "$editUrl/$id",
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      final data = response.data;
      print("data: $data");
      if(response.statusCode == 200){
        return Right(EditPost.fromJson(data));
      }
      return const Left("Can't edit post");
    } on DioError catch (e) {
      print("error: ${e.response}");
      if(e!.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      return Left(e.toString());
    }
  }

  Future<Either<String, String>> updatePost ({String? body, File? imagePath, required int id}) async {
    try{
      final response = await _dio.post(
        "$updateUrl/$id",
        data: FormData.fromMap({
          'body': body,
          'image': imagePath == null ? "" : await MultipartFile.fromFile(imagePath!.path)
        }),
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      if(response.statusCode == 200){
        return const Right("Update Success");
      }
      return const Left("Can't update post");
    } on DioError catch (e) {
      print("error: $e");
      if(e!.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      return Left(e.toString());
    }
  }

  Future<Either<String, String>> likePost ({required int id}) async {
    try{
      final response = await _dio.post(
        likeUrl,
        data: {
          "post_id": id
        },
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      if(response.statusCode == 200){
        return const Right("Like Success");
      }
      return const Left("Can't like post");
    } on DioError catch (e) {
      print("error: $e");
      if(e!.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      return Left(e.toString());
    }
  }

  Future<Either<String, String>> unLikePost ({required int id}) async{
    try{
      final response = await _dio.post(
          unlikeUrl,
          data: {
            "post_id": id
          },
          options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer $token"
              }
          )
      );
      if(response.statusCode == 200){
        return const Right("UnLike Success");
      }
      return const Left("Can't unlike post");
    } on DioError catch (e) {
      print("error: $e");
      if(e!.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      return Left(e.response!.data!["message"].toString());
    }
  }
}