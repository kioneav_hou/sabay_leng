import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:sabay_leng/models/comment_model.dart';
import 'package:sabay_leng/models/reply_comment_model.dart';
import 'package:sabay_leng/services/api_references.dart';

import '../controllers/user_global_controller.dart';
import '../models/failed_model.dart';

class ApiComment {
  final Dio _dio = Dio();
  final ReferenceApi _referenceApi = ReferenceApi();
  final UserGlobalController userGlobalController = UserGlobalController();
  late final String baseUrl = _referenceApi.baseUrl;
  late final String token = _referenceApi.token;
  late final String urlGetComment = "$baseUrl/comment";
  late final String urlGetReplyComment = "$baseUrl/get/reply/comment";
  late final String addCommentUrl = "$baseUrl/comment/post";
  late final String editCommentUrl = "$baseUrl/edit/comment/post/";
  late final String updateCommentUrl = "$baseUrl/update/comment/post/";
  late final String deleteCommentUrl = "$baseUrl/delete/comment/post/";
  late final String urlReplyComment = "$baseUrl/reply/comment";

  Future<Either<String, CommentModel>> getComment ({required int postId}) async {
    try{
      final response = await _dio.get(
          "$urlGetComment/$postId",
          options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer $token"
              }
          )
      );
      final data = response.data;
      if(response.statusCode == 200){
        return Right(CommentModel.fromJson(data));
      }
      return const Left("Error");
    } on DioError catch (e) {
      if(e.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      print("error: $e");
      return Left(e.response.toString());
    }
  }

  Future<Either<String, ReplyCommentModel>> getReplyComment ({required int commentId}) async {
    try{
      final response = await _dio.get(
        "$urlGetReplyComment/$commentId",
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      final data = response.data;
      if(response.statusCode == 200){
        return Right(ReplyCommentModel.fromJson(data));
      }
      return const Left("Error");
    } on DioError catch (e) {
      if(e.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      print("error: $e");
      return Left(e.response.toString());
    }
  }

  Future<Either<String, String>> postComment ({required int postId, required String comment}) async {
    try{
      final response = await _dio.post(
          addCommentUrl,
          data: {
            "post_id": postId,
            "text": comment
          },
          options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer $token"
              }
          )
      );
      final data = response.data;
      print("data: $data, status: ${response.statusCode}");
      if(response.statusCode == 200){
        return const Right("Success");
      }
      return const Left("Can't comment");
    } on DioError catch (e) {
      print("error comment : ${e}");
      if(e.response!.statusCode == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      final data = FailedModel.fromJson(jsonDecode(e.response!.toString()));
      print("error : $data");
      return Left(data!.failed![0].toString());
    }
  }

  Future<Either<String, dynamic>> editComment ({required int id}) async {
    try{
      final response = await _dio.post(
        "$editCommentUrl$id",
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      final data = response.data;
      if(response.statusCode == 200){
        return Right(data);
      }
      return const Left("Error");
    } on DioError catch (e) {
      if(e.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      print("error: $e");
      return Left(e.response.toString());
    }
  }

  Future<Either<String, String>> updateComment ({required int id, required String comment}) async {
    try{
      final response = await _dio.post(
        "$updateCommentUrl$id",
        data: {
          "text": comment
        },
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      if(response.statusCode == 200){
        return const Right("Success");
      }
      return const Left("Error");
    } on DioError catch (e) {
      if(e.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      print("error: $e");
      return Left(e.response.toString());
    }
  }

  Future<Either<String, String>> deleteComment ({required int id}) async {
    try{
      final response = await _dio.post(
          "$deleteCommentUrl$id",
          options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer $token"
              }
          )
      );
      if(response.statusCode == 200){
        return const Right("Deelete Success");
      }
      return const Left("Error");
    } on DioError catch (e) {
      if(e.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      print("error: $e");
      return Left(e.response.toString());
    }
  }

  Future<Either<String, String>> replyComment ({required int commentId, required String replyText}) async {
    try{
      final response = await _dio.post(
        urlReplyComment,
        data: {
          "comment_id": commentId,
          "text": replyText
        },
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }
        )
      );
      if(response.statusCode == 200){
        return const Right("Success");
      }
      return const Left("Error");
    }on DioError catch (e) {
      if(e.response!.statusCode! == 401 && e.response!.data!["message"] == "Token has expired"){
        await userGlobalController.revokeTokenUser();
        return const Left("Token Expired");
      }
      print("error: $e");
      return Left(e.response.toString());
    }
  }
}