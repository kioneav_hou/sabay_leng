class FailedModel {
  List<String>? failed;

  FailedModel({this.failed});

  FailedModel.fromJson(Map<String, dynamic> json) {
    failed = json['failed'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['failed'] = this.failed;
    return data;
  }
}