import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

//import bottom navigation screen
import 'auth/login_screen.dart';
import './fire_base_option.dart';
import 'bottom_navigation/bottom_screen.dart';
import 'controllers/connection_manager_controller.dart';
import 'controllers/controller_binding.dart';

import 'package:firebase_core/firebase_core.dart';


void main() async {
  await GetStorage.init();
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  // We're using the manual installation on non-web platforms since Google sign in plugin doesn't yet support Dart initialization.
  // See related issue: https://github.com/flutter/flutter/issues/96391

  // We store the app and auth to make testing with a named instance easier.
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: ControllerBinding(),
      title: 'Flutter Demo',
      home: const HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

//create screen test
// class TestPage extends StatefulWidget {
//   const TestPage({super.key});
//
//   @override
//   State<TestPage> createState() => _TestPageState();
// }

// class _TestPageState extends State<TestPage> {
//   final _dio = Dio();
//   final ReferenceApi referenceApi = ReferenceApi();
//   late String token = referenceApi.token;
//   @override
//   initState() {
//     FlutterNativeSplash.remove();
//     super.initState();
//   }
//
//   Stream<dynamic> getRandomNumberFact() async* {
//
//     yield* Stream.periodic(Duration(seconds: 5), (_) async {
//
//       return await _dio.get(
//           "http://192.168.88.64:8000/api/get/posts",
//           options: Options(
//               headers: {
//                 "Accept": "application/json",
//                 "Authorization": "Bearer $token"
//               }
//           )
//       );
//     }).asyncMap((event) async => await event);
//   }
//
//   @override
//
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Test Page"),
//       ),
//       body: Center(
//         child: StreamBuilder<dynamic>(
//           stream: getRandomNumberFact(),
//           builder: (context, snapshot) => snapshot.hasData
//               ? Center(child: Text(snapshot.data.toString()))
//               : CircularProgressIndicator(),
//         ),
//       ),
//     );
//   }
// }

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ConnectionManagerController _controller = Get.find<ConnectionManagerController>();
  final _storage = GetStorage();
  late var _componentScreen;

  @override
  void initState() {
    print("accessToken: ${_storage.read("accessToken")}");
    setState(() {
      _componentScreen =  _storage.read("accessToken") != null
          ? const BottomNavigationScreen()
          : LoginScreen();
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: StreamBuilder<ConnectivityResult>(
      //     stream: _connectivity.stream,
      //     builder: (context, snapshot){
      //       FlutterNativeSplash.remove();
      //
      //       if(snapshot.hasData){
      //         final result = snapshot.data;
      //         if(result == ConnectivityResult.none){
      //           // return Center(
      //           //   child: Container(
      //           //     child: Image.asset(
      //           //       "images/no_internet1.jpeg",
      //           //       fit: BoxFit.cover,
      //           //       height: Get.height,
      //           //     ),
      //           //   ),
      //           // );
      //           return const Center(
      //             child: CircularProgressIndicator(),
      //           );
      //         }else{
      //           return const BottomNavigationScreen();
      //         }
      //       }else{
      //         return const Center(
      //           child: CircularProgressIndicator(),
      //         );
      //       }
      //     }
      // ),
      body: Center(
        child: Obx(() =>
            _controller.connectionType.value == 1
              ? _componentScreen
              : _controller.connectionType.value == 2
              ? _componentScreen
              : const Text("No Internet"),
        ),
      ),
    );
  }
}



